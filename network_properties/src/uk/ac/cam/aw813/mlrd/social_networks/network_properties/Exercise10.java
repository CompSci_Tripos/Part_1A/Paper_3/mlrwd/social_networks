package uk.ac.cam.aw813.mlrd.social_networks.network_properties;

import uk.ac.cam.cl.mlrd.exercises.social_networks.IExercise10;

import java.io.IOException;
import java.nio.file.Path;
import java.util.*;

public class Exercise10 implements IExercise10 {
    @Override
    public Map<Integer, Set<Integer>> loadGraph(Path graphFile) throws IOException {
        return GraphUtils.loadGraph(graphFile);
    }

    @Override
    public Map<Integer, Integer> getConnectivities(Map<Integer, Set<Integer>> graph) {
        Map<Integer, Integer> ret = new HashMap<>();
        for (Map.Entry<Integer, Set<Integer>> vertex : graph.entrySet()) {
            ret.put(vertex.getKey(), vertex.getValue().size());
        }
        return ret;
    }

    @Override
    public int getDiameter(Map<Integer, Set<Integer>> graph) {
        int maxDistance = 0;
        Set<Integer> vertices = graph.keySet();
        for (Integer start : vertices) {
            maxDistance = Math.max(maxDistance, bfsMaxPathLength(graph, start));
        }

        return maxDistance;
    }

    private int bfsMaxPathLength(Map<Integer, Set<Integer>> graph, int start) {
        Set<Integer> seenVertices = new HashSet<>();
        Queue<Integer> toExplore = new LinkedList<>();
        Map<Integer, Integer> distances = new HashMap<>();
        HashSet<Integer> noNeighbours = new HashSet<>();
        toExplore.add(start);
        seenVertices.add(start);
        distances.put(start, 0);

        Integer exploring;
        while ((exploring = toExplore.poll()) != null) {
            for (Integer neighbour : graph.getOrDefault(exploring, noNeighbours)) {
                if (seenVertices.add(neighbour)) {
                    distances.put(neighbour, distances.get(exploring) + 1);
                    toExplore.add(neighbour);
                }
            }
        }

        return distances.values().stream().mapToInt(Integer::intValue).max().orElseThrow(
            () -> new RuntimeException("NO MAX INT?!?"));
    }
}
