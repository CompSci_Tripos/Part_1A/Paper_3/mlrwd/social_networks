package uk.ac.cam.aw813.mlrd.social_networks.network_properties;

import java.io.BufferedReader;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

class GraphUtils {
    static Map<Integer, Set<Integer>> loadGraph(Path graphFile) throws IOException {
        Map<Integer, Set<Integer>> ret = new HashMap<>();

        BufferedReader edgeCSV = Files.newBufferedReader(graphFile);
        String edge;
        while ((edge = edgeCSV.readLine()) != null) {
            String[] vertexPair = edge.split(" ");
            Integer vertex1 = Integer.parseInt(vertexPair[0]);
            Integer vertex2 = Integer.parseInt(vertexPair[1]);
            ret.putIfAbsent(vertex1, new HashSet<>());
            ret.get(vertex1).add(vertex2);
            ret.putIfAbsent(vertex2, new HashSet<>());
            ret.get(vertex2).add(vertex1);
        }
        return ret;
    }
}
