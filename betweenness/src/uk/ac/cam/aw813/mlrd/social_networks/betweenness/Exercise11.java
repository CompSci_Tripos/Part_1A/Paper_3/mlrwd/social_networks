package uk.ac.cam.aw813.mlrd.social_networks.betweenness;

import uk.ac.cam.cl.mlrd.exercises.social_networks.IExercise11;

import java.io.IOException;
import java.nio.file.Path;
import java.util.*;

public class Exercise11 implements IExercise11 {
    @SuppressWarnings("ConstantConditions")
    @Override
    public Map<Integer, Double> getNodeBetweenness(Path graphFile) throws IOException {
        Map<Integer, Set<Integer>> graph = GraphUtils.loadGraph(graphFile);
        Map<Integer, Double> betweenness = new HashMap<>();
        for (Integer vertex : graph.keySet()) {
            betweenness.put(vertex, 0.0);
        }

        for (Integer source : graph.keySet()) {
            // Single-source shortest paths problem
            Queue<Integer> vertexQueue = new LinkedList<>();
            Stack<Integer> vertexStack = new Stack<>();

            // Initialise all vertex properties according to Brandes' algorithm
            Map<Integer, Integer> distance = new HashMap<>();
            Map<Integer, List<Integer>> predecessors = new HashMap<>();
            Map<Integer, Double> sigma = new HashMap<>();
            for (Integer vertex : graph.keySet()) {
                predecessors.put(vertex, new ArrayList<>());
                distance.put(vertex, Integer.MAX_VALUE);
                sigma.put(vertex, 0.0);
            }
            distance.put(source, 0);
            sigma.put(source, 1.0);
            vertexQueue.add(source);

            Integer currentV;

            while ((currentV = vertexQueue.poll()) != null) {
                vertexStack.push(currentV);
                Integer vDistance = distance.get(currentV);
                Double vSigma = sigma.get(currentV);
                for (Integer neighbour : graph.get(currentV)) {
                    // Path discovery
                    if (distance.get(neighbour) == Integer.MAX_VALUE) {
                        distance.put(neighbour, vDistance + 1);
                        vertexQueue.add(neighbour);
                    }

                    // Path counting
                    if (distance.get(neighbour) == vDistance + 1) {
                        sigma.compute(neighbour, (k, v) -> v + vSigma);
                        predecessors.get(neighbour).add(currentV);
                    }
                }
            }

            // Accumulation
            Map<Integer, Double> delta = new HashMap<>();
            for (Integer vertex : graph.keySet()) {
                delta.put(vertex, 0.0);
            }

            while (!vertexStack.empty()) {
                currentV = vertexStack.pop();
                final double currentVSigma = sigma.get(currentV);
                final double currentVDelta = delta.get(currentV);
                for (Integer predecessor : predecessors.get(currentV)) {
                    final double predecessorSigma = sigma.get(predecessor);
                    delta.compute(
                        predecessor, (k, v) -> v + (predecessorSigma / currentVSigma) * (1.0 + currentVDelta));
                }
                if (!currentV.equals(source)) {
                    betweenness.compute(currentV, (k, v) -> v + currentVDelta);
                }
            }
        }
        betweenness.replaceAll((k, v) -> v / 2.0);
        return betweenness;
    }
}
