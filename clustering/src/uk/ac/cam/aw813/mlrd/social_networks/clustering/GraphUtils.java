package uk.ac.cam.aw813.mlrd.social_networks.clustering;

import java.io.BufferedReader;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.*;

public class GraphUtils {
    public static Map<Integer, Set<Integer>> loadGraph(Path graphFile) throws IOException {
        Map<Integer, Set<Integer>> ret = new HashMap<>();
        BufferedReader edgeCSV = Files.newBufferedReader(graphFile);
        String edge;
        while ((edge = edgeCSV.readLine()) != null) {
            String[] vertexPair = edge.split(" ");
            Integer vertex1 = Integer.parseInt(vertexPair[0]);
            Integer vertex2 = Integer.parseInt(vertexPair[1]);
            ret.putIfAbsent(vertex1, new HashSet<>());
            ret.get(vertex1).add(vertex2);
            ret.putIfAbsent(vertex2, new HashSet<>());
            ret.get(vertex2).add(vertex1);
        }
        return ret;
    }

    public static int countEdges(Map<Integer, Set<Integer>> graph){
        return graph.values().stream().mapToInt(Collection::size).sum() / 2;
    }
}
