package uk.ac.cam.aw813.mlrd.social_networks.clustering;

import uk.ac.cam.cl.mlrd.exercises.social_networks.IExercise12;

import java.util.*;

public class Exercise12 implements IExercise12 {
    @Override
    public List<Set<Integer>> GirvanNewman(Map<Integer, Set<Integer>> graph, int minimumComponents) {
        List<Set<Integer>> graphComponents = getComponents(graph);
        while (graphComponents.size() < minimumComponents) {
            double maxEdgeBetweenness = 0.0;
            Map<Integer, Map<Integer, Double>> edgeBetweenness = getEdgeBetweenness(graph);
            for (Integer v1 : edgeBetweenness.keySet()) {
                for (Integer v2 : edgeBetweenness.get(v1).keySet()) {
                    if (edgeBetweenness.get(v1).get(v2) > maxEdgeBetweenness) {
                        maxEdgeBetweenness = edgeBetweenness.get(v1).get(v2);
                    }
                }
            }

            for (Integer v1 : edgeBetweenness.keySet()) {
                for (Integer v2 : edgeBetweenness.get(v1).keySet()) {
                    if (fuzzyDoubleEquals(edgeBetweenness.get(v1).get(v2), maxEdgeBetweenness, 1e-6)) {
                        graph.get(v1).remove(v2);
                        graph.get(v2).remove(v1);
                    }
                }
            }

            graphComponents = getComponents(graph);
        }
        return graphComponents;
    }

    @SuppressWarnings("SameParameterValue")
    private boolean fuzzyDoubleEquals(double d1, double d2, double epsilon) {
        return Math.abs(d1 - d2) < epsilon;
    }


    @Override
    public int getNumberOfEdges(Map<Integer, Set<Integer>> graph) {
        return GraphUtils.countEdges(graph);
    }

    @Override
    public List<Set<Integer>> getComponents(Map<Integer, Set<Integer>> graph) {
        List<Set<Integer>> ret = new ArrayList<>();
        Set<Integer> seenVertices = new HashSet<>();
        while (graph.keySet().size() != seenVertices.size()) {
            Set<Integer> component = new HashSet<>();
            Stack<Integer> toExplore = new Stack<>();

            // Pick an unseen node to start with
            Set<Integer> stillToFind = new HashSet<>(graph.keySet());
            stillToFind.removeAll(seenVertices);
            Integer source = stillToFind.iterator().next();
            seenVertices.add(source);
            component.add(source);
            toExplore.push(source);

            while (!toExplore.empty()) {
                Integer currentV = toExplore.pop();
                for (Integer neighbour : graph.get(currentV)) {
                    if (seenVertices.add(neighbour)) {
                        component.add(neighbour);
                        toExplore.push(neighbour);
                    }
                }
            }

            ret.add(component);
        }
        return ret;
    }

    @Override
    public Map<Integer, Map<Integer, Double>> getEdgeBetweenness(Map<Integer, Set<Integer>> graph) {
        Map<Integer, Map<Integer, Double>> betweenness = new HashMap<>();
        for (Integer source : graph.keySet()) {
            // Single-source shortest paths problem
            Queue<Integer> vertexQueue = new LinkedList<>();
            Stack<Integer> vertexStack = new Stack<>();

            // Initialise all vertex properties according to Brandes' algorithm
            Map<Integer, Integer> distance = new HashMap<>();
            Map<Integer, List<Integer>> predecessors = new HashMap<>();
            Map<Integer, Double> sigma = new HashMap<>();
            for (Integer vertex : graph.keySet()) {
                predecessors.put(vertex, new ArrayList<>());
                distance.put(vertex, Integer.MAX_VALUE);
                sigma.put(vertex, 0.0);
            }
            distance.put(source, 0);
            sigma.put(source, 1.0);
            vertexQueue.add(source);

            Integer currentV;

            while ((currentV = vertexQueue.poll()) != null) {
                vertexStack.push(currentV);
                Integer vDistance = distance.get(currentV);
                Double vSigma = sigma.get(currentV);
                for (Integer neighbour : graph.get(currentV)) {
                    // Path discovery
                    if (distance.get(neighbour) == Integer.MAX_VALUE) {
                        distance.put(neighbour, vDistance + 1);
                        vertexQueue.add(neighbour);
                    }

                    // Path counting
                    if (distance.get(neighbour) == vDistance + 1) {
                        //noinspection ConstantConditions
                        sigma.compute(neighbour, (k, v) -> v + vSigma);
                        predecessors.get(neighbour).add(currentV);
                    }
                }
            }

            // Accumulation
            Map<Integer, Double> delta = new HashMap<>();
            for (Integer vertex : graph.keySet()) {
                delta.put(vertex, 0.0);
            }


            while (!vertexStack.empty()) {
                currentV = vertexStack.pop();
                final double currentVSigma = sigma.get(currentV);
                final double currentVDelta = delta.get(currentV);
                for (Integer predecessor : predecessors.get(currentV)) {
                    final double predecessorSigma = sigma.get(predecessor);
                    final double c = (predecessorSigma / currentVSigma) * (1 + currentVDelta);
                    updateEdgeBetweenness(betweenness, currentV, predecessor, c);
                    //noinspection ConstantConditions
                    delta.compute(predecessor, (k, v) -> v + c);
                }

            }
        }
        for (Map<Integer, Double> column : betweenness.values()) {
            column.replaceAll((k, v) -> v / 2);
        }

        return betweenness;
    }

    private void updateEdgeBetweenness(Map<Integer, Map<Integer, Double>> betweenness, Integer v1, Integer v2, double c) {
        betweenness.putIfAbsent(v1, new HashMap<>());
        betweenness.get(v1).compute(v2, (k, v) -> v == null ? c : v + c);
        betweenness.putIfAbsent(v2, new HashMap<>());
        betweenness.get(v2).compute(v1, (k, v) -> v == null ? c : v + c);

    }
}
